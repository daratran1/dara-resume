import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UtilsModule} from '../utils/utils.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from '../app-rooting.module';

@NgModule({
  declarations: [],
  exports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    UtilsModule,
    AppRoutingModule
  ]
})
export class SharedModule { }
