import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentSkillsComponent } from './content-skills.component';

describe('ContentSkillsComponent', () => {
  let component: ContentSkillsComponent;
  let fixture: ComponentFixture<ContentSkillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentSkillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentSkillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
