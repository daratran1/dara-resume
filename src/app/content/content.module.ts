import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {MenuModule} from '../menu/menu.module';
import {ContentContactsComponent} from './content-contacts/content-contacts.component';
import {ContentEducationsComponent} from './content-educations/content-educations.component';
import {ContentExperiencesComponent} from './content-experiences/content-experiences.component';
import {ContentMainComponent} from './content-main/content-main.component';
import {ContentSkillsComponent} from './content-skills/content-skills.component';
import { ContentDetailsComponent } from './content-details/content-details.component';

@NgModule({
  declarations: [
    ContentContactsComponent,
    ContentEducationsComponent,
    ContentExperiencesComponent,
    ContentMainComponent,
    ContentSkillsComponent,
    ContentDetailsComponent,
  ],
  imports: [
    SharedModule, MenuModule
  ]
})
export class ContentModule {
}
