import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentExperiencesComponent } from './content-experiences.component';

describe('ContentExperiencesComponent', () => {
  let component: ContentExperiencesComponent;
  let fixture: ComponentFixture<ContentExperiencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentExperiencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentExperiencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
