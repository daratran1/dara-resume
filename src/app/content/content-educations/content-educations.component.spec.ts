import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentEducationsComponent } from './content-educations.component';

describe('ContentEducationsComponent', () => {
  let component: ContentEducationsComponent;
  let fixture: ComponentFixture<ContentEducationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentEducationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentEducationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
