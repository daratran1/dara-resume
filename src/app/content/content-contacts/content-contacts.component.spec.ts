import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentContactsComponent } from './content-contacts.component';

describe('ContentContactsComponent', () => {
  let component: ContentContactsComponent;
  let fixture: ComponentFixture<ContentContactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentContactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
