export interface AppEducation {
  id?: number;
  diplomaName: string;
  schoolName: string;
  date: {
    start: Date;
    end: Date;
  };
}
