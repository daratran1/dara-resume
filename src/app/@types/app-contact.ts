export interface AppContact {
  id?: number;
  fullName: string;
  address: string;
  phone: string;
  mail: string;
  linkedin?: string;
  gitlab?: string;
  description?: string;
  languages: string[];
  avatar?: {
    source: string;
    name: string;
  };
}
