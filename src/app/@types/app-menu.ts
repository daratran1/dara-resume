export interface AppMenu {
  id?: number;
  name: string;
  path: string;
  subMenu?: AppMenu[];
}
