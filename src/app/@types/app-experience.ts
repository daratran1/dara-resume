export interface AppExperience {
  id?: number;
  companyName: string;
  jobName: string;
  date: {
    start: Date,
    end: Date
  };
  description?: string;
  tasks: string[];
}
