export enum Menu {
  HOME = 1,
  PERSONAL_DETAIL = 2,
  EDUCATION = 3,
  EXPERIENCE = 4,
  SKILL = 5
}
