import {NgModule} from '@angular/core';
import {MenuComponent} from './menu/menu.component';
import {MenuListComponent} from './menu-list/menu-list.component';
import {MenuListItemComponent} from './menu-list-item/menu-list-item.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    MenuComponent,
    MenuListComponent,
    MenuListItemComponent,
  ],
  imports: [SharedModule],
  exports: [MenuComponent]
})
export class MenuModule {
}
