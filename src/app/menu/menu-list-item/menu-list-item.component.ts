import {Component, Input} from '@angular/core';
import {AppMenu} from '../../@types/app-menu';
import {menus} from '../../services/mock-data';


@Component({
  selector: 'app-menu-list-item',
  templateUrl: './menu-list-item.component.html',
  styleUrls: ['./menu-list-item.component.css']
})
export class MenuListItemComponent {

  @Input() menu: AppMenu = menus[0];
}
