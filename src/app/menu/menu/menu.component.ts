import {Component, Output} from '@angular/core';
import {AppMenu} from '../../@types/app-menu';
import {menus} from '../../services/mock-data';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  @Output() selectedMenu: AppMenu = menus[0];

}
