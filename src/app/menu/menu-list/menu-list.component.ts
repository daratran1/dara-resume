import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {menus} from 'src/app/services/mock-data';
import {AppMenu} from '../../@types/app-menu';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent {

  @Input() collection: AppMenu[] = menus;
  @Input() selection: AppMenu = menus[0];
  @Output() action: EventEmitter<AppMenu> = new EventEmitter();


}
