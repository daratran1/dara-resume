import {AppExperience} from '../../@types/app-experience';

export const experiencesEN: AppExperience[] = [{
    id: 1,
    companyName: 'Prima Solutions',
    jobName: 'Java Software Developer',
    date: {
      start: new Date(2017, 1),
      end: new Date(2019, 1)
    },
    description: 'llorem descriptionqsdqsdlqskdjlqksdjqlskdjqskldjsqkdjsqkdjqslkdj',
    tasks: ['Fix bugs', 'Develop new features', 'Write test cases', 'Code review', 'IT tasks, release versions']
  }, {
    id: 2,
    companyName: 'Canal+',
    jobName: 'Software Agil Test Developer',
    date: {
      start: new Date(2014, 1),
      end: new Date(2017, 1)
    },
    description: 'bla bla bla canal llorem ipsum',
    tasks: ['Develop automatisation test with Cucumber JVM, Concordion',
      'Develop a data generator (creation of contracts ..)',
      'Implement REST web services',
      'Write agil test cases (BDD). JIRA, Confluence',
      'Jenkins for continuous integration']
  }, {
    id: 3,
    companyName: 'BeeToBee',
    jobName: 'Java Software Developer (part time student)',
    date: {
      start: new Date(2012, 1),
      end: new Date(2014, 1)
    },
    description: 'BeeToBee llorem ipsum lakzjekazjlazjkelkazjelkazjelkazeazlk',
    tasks: ['Design and implement a documentation web site using Java, Spring 3, JSF, Hibernate, Mysql 5.0.',
      'Fix bugs',
      'Develop new features',
      'Convert website to an Android application using Cordova API.',
      'Use Jasper IReport to produce invoice, proposals and contract documents.']
  }, {
    id: 4,
    companyName: 'Neopost',
    jobName: 'BA/QA analyst',
    date: {
      start: new Date(2009, 1),
      end: new Date(2010, 1)
    },
    description: 'Neopost',
    tasks: ['Write specifications and test cases for several industrial printer.',
      'Use with HP Quality center']
  }
  ]
;
