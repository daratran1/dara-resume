import {AppMenu} from '../../@types/app-menu';

export const menus: AppMenu[] = [
  {
    id: 1,
    name: 'HOME',
    path: '/content/main'
  },
  {
    id: 1,
    name: 'CONTACT',
    path: '/content/contacts'
  },
  {
    id: 1,
    name: 'EDUCATION',
    path: '/content/educations'
  },
  {
    id: 1,
    name: 'EXPERIENCES',
    path: '/content/experiences',
    subMenu: [
      {
        id: 1,
        name: 'PRIMA SOLUTIONS',
        path: ''
      },
      {
        id: 1,
        name: 'CANAL +',
        path: ''
      },
      {
        id: 1,
        name: 'BEE TO BEE',
        path: ''
      }
    ]
  },
  {
    id: 1,
    name: 'SKILLS',
    path: '/content/skills'
  }
];
