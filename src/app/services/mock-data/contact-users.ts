import {AppContact} from 'src/app/@types/app-contact';

export const contactUser: AppContact = {
  id: 1,
  fullName: 'Dara TRAN',
  address: 'Paris',
  phone: '0611815567',
  mail: 'daratran@gmail.com',
  linkedin: '',
  gitlab: '',
  description: 'Llorem ipsoum',
  languages: ['Anglais', 'Français'],
};
