import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContentMainComponent} from './content/content-main/content-main.component';
import {ContentEducationsComponent} from './content/content-educations/content-educations.component';
import {ContentExperiencesComponent} from './content/content-experiences/content-experiences.component';
import {ContentSkillsComponent} from './content/content-skills/content-skills.component';
import {ContentContactsComponent} from './content/content-contacts/content-contacts.component';
import {ContentDetailsComponent} from './content/content-details/content-details.component';


const routes: Routes = [
  {path: '', redirectTo: '/content/main', pathMatch: 'full'},
  {path: 'content', component: ContentMainComponent},
  {path: 'content/main', component: ContentMainComponent},
  {path: 'content/educations', component: ContentEducationsComponent},
  {path: 'content/details', component: ContentDetailsComponent},
  {path: 'content/experiences', component: ContentExperiencesComponent},
  {path: 'content/skills', component: ContentSkillsComponent},
  {path: 'content/contacts', component: ContentContactsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
